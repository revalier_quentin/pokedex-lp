import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {NavigationContainer} from "@react-navigation/native";
import Home from "../Screens/Home";
import PokemonDetails from "../Screens/PokemonDetails";
import AbilityDetails from "../Screens/AbilityDetails";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Settings from "../Screens/Settings";
import Search from "../Screens/Search";
import Teams from "../Screens/Teams";

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function PokemonStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}>
            <Stack.Screen name="Home" component={Home}/>
            <Stack.Screen name="PokemonDetails" component={PokemonDetails}/>
            <Stack.Screen name="AbilityDetails" component={AbilityDetails}/>
        </Stack.Navigator>
    )
}

function SearchStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}>
            <Stack.Screen name="Search" component={Search}/>
            <Stack.Screen name="PokemonDetails" component={PokemonDetails}/>
            <Stack.Screen name="AbilityDetails" component={AbilityDetails}/>
        </Stack.Navigator>
    )
}

function TeamsStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}>
            <Stack.Screen name="Teams" component={Teams}/>
            <Stack.Screen name="PokemonDetails" component={PokemonDetails}/>
            <Stack.Screen name="AbilityDetails" component={AbilityDetails}/>
        </Stack.Navigator>
    )
}

export default function Navigation() {
    return (
        <NavigationContainer>
            <Tab.Navigator
                activeColor="black"
                barStyle={{ backgroundColor: 'tomato' }}
                screenOptions={{
                    headerShown: false
                }}>
                <Tab.Screen name="Home" component={PokemonStack} options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="home" color={color} size={26} />
                    ),
                }}/>
                <Tab.Screen name="Search" component={SearchStack} options={{
                    tabBarLabel: 'Search',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="magnify" color={color} size={26} />
                    ),
                }}/>
                <Tab.Screen name="Teams" component={TeamsStack} options={{
                    tabBarLabel: 'Teams',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="account-group" color={color} size={26} />
                    ),
                }}/>
                <Tab.Screen name="Settings" component={Settings} options={{
                    tabBarLabel: 'Settings',
                    tabBarIcon: ({ color }) => (
                        <MaterialCommunityIcons name="cog" color={color} size={26} />
                    ),
                }}/>
            </Tab.Navigator>
        </NavigationContainer>
    )
}