import {StyleSheet, Button, View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import {getPokemon} from "../Api/PokeApi";
import React, {useEffect, useState} from "react";
import Ability from "../Components/Ability";
import Specie from "../Components/Specie";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function PokemonDetails(props) {

    const [PokemonInfos, setPokemonInfos] = useState(null);
    const [PokemonImg, setPokemonImg] = useState(null);
    const {route, navigation, ...restProps} = props
    const {url, name} = route.params;


    const storeData = async (key, value) => {
        try {
            const res = [];
            const getValue = JSON.parse(await AsyncStorage.getItem(key));
            if (getValue){
                for (let i = 0; i < getValue.length; i++) {
                    res.push(getValue[i]);
                }
                if (getValue.length > 5){
                    res.pop();
                }
            }
            res.push(value);
            const jsonValue = JSON.stringify(res)
            await AsyncStorage.setItem(key, jsonValue)
        } catch (e) {
            console.log("erreur : ",e);
        }
    }

    const Capitalize = (str) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    useEffect(() => {
        loadPokemon(url)
    }, [])

    const loadPokemon = (url) => {
        getPokemon(url).then(datas => {
            setPokemonInfos(datas);
            storeData("@team_poke", {name : datas.name}).then(r => console.log("Pokemon added to the Team !", {name : datas.name}));
            setPokemonImg(datas.sprites.front_default);
        })
    }

    return (

        <View style={styles.container}>
            <Text style={styles.title}>{Capitalize(name)}</Text>
            <View style={styles.imgContainer}>
                <Image source={{uri: PokemonImg}}
                       style={{width: 200, height: 200, margin: "auto"}}/>
            </View>
            <Text>Species : </Text>
            {PokemonInfos ?
                <>
                    <Specie url={PokemonInfos.species.url}/>
                    <Text>Abilities : </Text>
                    <FlatList
                        style={styles.pokeList}
                        contentContainerStyle={styles.pokeListItem}
                        numColumns={2}
                        data={PokemonInfos.abilities}
                        keyExtractor={(item => {
                            item.ability.name
                        })}
                        renderItem={({item}) =>
                            <TouchableOpacity style={styles.pokemon} onPress={() => navigation.navigate('AbilityDetails', {
                                name: item.ability.name,
                                url: item.ability.url
                            })}>
                                <Text>{item.ability.name}</Text>
                            </TouchableOpacity>
                        }
                    />
                </>

            : null}
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        paddingTop: 50,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold"
    },
    imgContainer: {
        width: "90%",
        height: 200,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "red",
        borderWidth: 2,
        margin: 10,
        borderRadius: 15
    },
    pokeList: {
        display: "flex",
        borderColor: "blue",
        borderWidth: 2,
        flexGrow: 1,
        width: "100%",
    },
    pokeListItem: {
        justifyContent: "space-between"
    },
    pokemon: {
        borderWidth: 2,
        width: "45%",
        height: 40,
        borderColor: "green",
        margin: 10,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15
    }
});