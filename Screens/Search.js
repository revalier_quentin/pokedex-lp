// Search.js
import React, {useState, useEffect} from "react";
import {
    StyleSheet,
    Text,
    SafeAreaView,
    ActivityIndicator,
} from "react-native";
import List from "../Components/List";
import SearchBar from "../Components/SearchBar";
import {getPokemon} from "../Api/PokeApi";

const Search = (props) => {
    const {navigation, ...restProps} = props
    const [searchPhrase, setSearchPhrase] = useState("");
    const [clicked, setClicked] = useState(false);
    const [listPokemon, setListPokemon] = useState([]);
    const [nextPokemonLink, setNextPokemonLink] = useState("https://pokeapi.co/api/v2/pokemon");

    useEffect(() => {
        loadPokemon(nextPokemonLink);
    }, []);

    const loadPokemon = (url) => {
        getPokemon(url).then(datas => {
            setListPokemon([...listPokemon, ...datas.results]);
            setNextPokemonLink(datas.next);
            loadPokemon(nextPokemonLink);
        })
    }

    return (
        <SafeAreaView style={styles.root}>
            {!clicked && <Text style={styles.title}>Pokemon Search</Text>}
            <SearchBar
                searchPhrase={searchPhrase}
                setSearchPhrase={setSearchPhrase}
                clicked={clicked}
            />
            {(

                <List
                    searchPhrase={searchPhrase}
                    data={listPokemon}
                    navigation={navigation}
                />

            )}
        </SafeAreaView>
    );
};

export default Search;

const styles = StyleSheet.create({
    root: {
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        width: "100%",
        marginTop: 100,
        fontSize: 25,
        fontWeight: "bold",
        marginLeft: "10%",
    },
});