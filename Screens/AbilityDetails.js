import {StyleSheet, Button, View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import {getPokemon} from "../Api/PokeApi";
import React, {useEffect, useState} from "react";
import Ability from "../Components/Ability";

export default function AbilityDetails(props) {

    const {route, navigation, ...restProps} = props
    const {url, name} = route.params;

    const Capitalize = (str) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    return (

        <View style={styles.container}>
            <Text style={styles.title}>{Capitalize(name)}</Text>
            <Ability url={url} />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        paddingTop: 50,
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        borderWidth: 2,
        borderColor: "blue"
    },
    title: {
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold"
    },
});