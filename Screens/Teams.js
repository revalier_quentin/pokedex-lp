import React, {useEffect, useState} from "react";
import {FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

const Teams = (props) => {

    const [team, setTeam] = useState([]);
    useEffect(() => {
        //Set up team from asyncStorage
        console.log("Team : ");
        console.log(team);
    }, []);

    const refresh = async () => {
        getData("@team_poke").then(r => setTeam(r));
        console.log("Resfresh of Team done !")
        console.log(team);
    }

    const reset = async () => {
        await AsyncStorage.clear();
        console.log("Team was reset !")
    }

    const getData = async (key) => {
        try {
            const jsonValue = await AsyncStorage.getItem(key)
            //console.log("GetData JSON : ");
            //console.log(jsonValue);
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            // error reading value
        }
    }

    return (
        <SafeAreaView style={styles.root}>
            <Text>The POKETEAM</Text>
            {team ?
                <>
                    <FlatList
                        style={styles.pokeList}
                        contentContainerStyle={styles.pokeListItem}
                        data={team}
                        keyExtractor={(item => item.name)}
                        numColumns={2}
                        renderItem={({item}) =>
                            <TouchableOpacity style={styles.pokemon}>
                                <Text style={styles.tilePokemon}>{item.name}</Text>
                            </TouchableOpacity>}
                    />
                </>
                : null}
                <TouchableOpacity style={styles.bottomButton}
                                  onPress={() => refresh()}><Text>REFRESH</Text></TouchableOpacity>
                <TouchableOpacity style={styles.bottomButton}
                                  onPress={() => reset()}><Text>RESET</Text></TouchableOpacity>
        </SafeAreaView>
    );
};

export default Teams;

const styles = StyleSheet.create({
    root: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 60,
    },
    title: {
        width: "100%",
        marginTop: 100,
        fontSize: 25,
        fontWeight: "bold",
        marginLeft: "10%",
    },
    pokeListItem: {
        justifyContent: "space-between"
    },
    pokeList: {
        display: "flex",
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey',
    },
    pokemon: {
        borderWidth: 2,
        width: "45%",
        height: 180,
        borderColor: "white",
        padding: 10,
        margin: 10,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    bottomButton: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 2,
        width: "90%",
        height: 60,
        textAlign: "center"
    },
});