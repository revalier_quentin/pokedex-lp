import React, {useState, useEffect} from 'react';
import {FlatList, Image, StyleSheet, Text, View, ActivityIndicator, TouchableOpacity} from 'react-native';

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function Settings(props) {

    const listSettings = [{name: "Profil", iconName: "account"}, {name: "Screen options", iconName: "fullscreen"}, {
        name: "Pokemon Guide",
        iconName: "book-open-page-variant"
    }, {name: "Privacy", iconName: "key-variant"}, {name: "Design", iconName: "pencil"}];

    return (
        <View style={styles.container}>
            <Text style={styles.titleSettings}>Mon pokedex</Text>
            <FlatList
                style={styles.settingsList}
                contentContainerStyle={styles.settingsListItem}
                data={listSettings}
                keyExtractor={(item => item.name)}
                renderItem={({item}) =>
                    <TouchableOpacity style={styles.settings}>
                        <MaterialCommunityIcons name={item.iconName} size={26}/>
                        <Text styles={styles.settingText}>{item.name}</Text>
                    </TouchableOpacity>}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        backgroundColor: '#e3350d',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1
    },
    titleSettings: {
        marginTop: 40,
        marginBottom: 20,
        fontSize: 18,
        fontWeight: "bold"
    },
    settingsList: {
        display: "flex",
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey',
    },
    settings: {
        display: "flex",
        flexDirection: "row",
        borderWidth: 2,
        alignSelf: 'stretch',
        height: 60,
        borderColor: "white",
        padding: 10,
        margin: 10,
        marginBottom: 5,
        textAlign: "left",
        justifyContent: "flex-start",
        alignItems: "center",
        borderRadius: 15,
        backgroundColor: "#F0F0F0"
    },
    settingsListItem: {
        justifyContent: "space-between"
    },
    settingText: {
        marginLeft: 30,
        borderWidth: 2,
        borderColor: "green"
    }
});
