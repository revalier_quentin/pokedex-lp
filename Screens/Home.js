import React, {useState, useEffect} from 'react';
import {StatusBar} from 'expo-status-bar';
import {FlatList, Image, StyleSheet, Text, View, ActivityIndicator, TouchableOpacity} from 'react-native';
import CustomButton from "../Components/Button";
import {getPokemon} from "../Api/PokeApi";
import logo from '../assets/porsche.png';
import Tile from "../Components/Tile";

export default function Home(props) {

    const {navigation, ...restProps} = props
    const [loader, setLoader] = useState(false);
    const [listPokemon, setListPokemon] = useState([]);
    const [nextPokemonLink, setNextPokemonLink] = useState("https://pokeapi.co/api/v2/pokemon");

    useEffect(() => {
        loadPokemon(nextPokemonLink);
    }, [])

    const loadPokemon = (url) => {
        if (!loader) {
            setLoader(true);
            getPokemon(url).then(datas => {
                setLoader(false);
                setListPokemon([...listPokemon, ...datas.results]);
                setNextPokemonLink(datas.next);
            })
        }
    }

    return (
        <View style={styles.container}>

            <Text style={styles.titlePokedex}>Mon pokedex</Text>
            <FlatList
                style={styles.pokeList}
                contentContainerStyle={styles.pokeListItem}
                data={listPokemon}
                keyExtractor={(item => item.name)}
                numColumns={2}
                renderItem={({item}) =>
                    <TouchableOpacity style={styles.pokemon} onPress={() => navigation.navigate('PokemonDetails', {
                        name: item.name,
                        url: item.url
                    })}>
                        <Tile style={styles.tilePokemon} name={item.name} url={item.url}/>
                    </TouchableOpacity>}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                    loadPokemon(nextPokemonLink)
                }}
            />
            <ActivityIndicator animating={loader} size="small" color="#0000ff"/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        backgroundColor: '#e3350d',
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1
    },
    pokeList: {
        display: "flex",
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey',
    },
    pokemon: {
        borderWidth: 2,
        width: "45%",
        height: 180,
        borderColor: "white",
        padding: 10,
        margin: 10,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    pokeListItem: {
        justifyContent: "space-between"
    },
    titlePokedex: {
        marginTop: 40,
        marginBottom: 20,
        fontSize: 18,
        fontWeight: "bold"
    },
    tilePokemon: {
        justifyContent: "center"
    }
});
