// List.js
import React from "react";
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    SafeAreaView, TouchableOpacity,
} from "react-native";
import Tile from "./Tile";

const List = ({ searchPhrase,  data, navigation }) => {
    const renderItem = ({ item }) => {
        // when no input, show all
        if (searchPhrase === "") {
            return <TouchableOpacity style={styles.pokemon} onPress={() => navigation.navigate('PokemonDetails', {
                name: item.name,
                url: item.url
            })}>
                <Tile style={styles.tilePokemon} name={item.name} url={item.url}/>
            </TouchableOpacity>;
        }
        // filter of the name
        if (item.name.toUpperCase().includes(searchPhrase.toUpperCase().trim().replace(/\s/g, ""))) {
            return <TouchableOpacity style={styles.pokemon} onPress={() => navigation.navigate('PokemonDetails', {
                name: item.name,
                url: item.url
            })}>
                <Tile style={styles.tilePokemon} name={item.name} url={item.url}/>
            </TouchableOpacity>;
        }
    };

    return (
        <SafeAreaView style={styles.list__container}>
            <View
            >
                <FlatList
                    style={styles.pokeList}
                    contentContainerStyle={styles.pokeListItem}
                    data={data}
                    numColumns={2}
                    keyExtractor={(item => item.name)}
                    renderItem={renderItem}
                    onEndReachedThreshold={0.5}
                />
            </View>
        </SafeAreaView>
    );
};

export default List;

const styles = StyleSheet.create({
    list__container: {
        margin: 10,
        height: "85%",
        width: "100%",
    },
    item: {
        margin: 30,
        borderBottomWidth: 2,
        borderBottomColor: "lightgrey"
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 5,
        fontStyle: "italic",
    },
    pokeList: {
        display: "flex",
        alignSelf: 'stretch',
        backgroundColor: 'lightgrey',
        marginBottom: 90
    },
    pokemon: {
        borderWidth: 2,
        width: "45%",
        height: 180,
        borderColor: "white",
        padding: 10,
        margin: 10,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    pokeListItem: {
        justifyContent: "space-between"
    },
    tilePokemon: {
        justifyContent: "center"
    }
});