import {StyleSheet, Button, View, Text, FlatList, Image} from 'react-native';
import pokeball from '../assets/pokeball.png';
import {getPokemon} from "../Api/PokeApi";
import React, {useEffect, useState} from "react";

export default function Ability(props) {

    const [PokemonAbility, setPokemonAbility] = useState(null);
    const {url, ...restProps} = props

    useEffect(async () => {
        loadPokemon(props.url)
    }, [])

    const loadPokemon = (url) => {
        getPokemon(url).then(datas => {
            setPokemonAbility(datas.effect_entries[1].short_effect);
        })
    }

    return (

        <View style={styles.container}>
            <Text style={{textAlign: "center"}}>{PokemonAbility}</Text>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
    },
});