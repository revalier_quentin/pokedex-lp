import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Button, View } from 'react-native';

export default function CustomButton(props) {

    const {text, color, onPressButton, ...restProps} = props

    return (
        <View style={styles.container}>
            <Button
                onPress={() => props.onPressButton(props.text)}
                title={props.text}
                color={props.color}
                accessibilityLabel="Learn more about this purple button"
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        marginTop: 10
    },
});
