import {StyleSheet, Button, View, Text, FlatList, Image, TouchableOpacity} from 'react-native';
import pokeball from '../assets/pokeball.png';
import {getPokemon} from "../Api/PokeApi";
import React, {useEffect, useState} from "react";

export default function Specie(props) {

    const [PokemonSpecie, setPokemonSpecie] = useState([]);
    const {url, ...restProps} = props

    useEffect(async () => {
        loadPokemon(props.url)
    }, [])

    const loadPokemon = (url) => {
        getPokemon(url).then(datas => {
            setPokemonSpecie(datas.egg_groups);
        })
    }

    return (

        <View style={styles.container}>
            <FlatList
                style={styles.pokeList}
                contentContainerStyle={styles.pokeListItem}
                numColumns={2}
                data={PokemonSpecie}
                keyExtractor={(item => {
                    item.name
                })}
                renderItem={({item}) =>
                    <TouchableOpacity style={styles.pokemon}>
                        <Text>{item.name}</Text>
                    </TouchableOpacity>
                }
            />
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
    },
    pokeList: {
        display: "flex",
        borderColor: "blue",
        borderWidth: 2,
        flexGrow: 1,
        width: "100%"
    },
    pokeListItem: {
        justifyContent: "space-between"
    },
    pokemon: {
        borderWidth: 2,
        width: "45%",
        height: 40,
        borderColor: "green",
        margin: 10,
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 15
    },
});