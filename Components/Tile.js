import {StyleSheet, Button, View, Text, FlatList, Image} from 'react-native';
import pokeball from '../assets/pokeball.png';
import {getPokemon} from "../Api/PokeApi";
import React, {useEffect, useState} from "react";

export default function Tile(props) {

    //const [PokemonInfos, setPokemonInfos] = useState([]);
    const [PokemonImg, setPokemonImg] = useState(null);
    const {url, name, ...restProps} = props

    const Capitalize = (str) => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    useEffect(async () => {
        loadPokemon(props.url)
    }, [])

    const loadPokemon = (url) => {
        getPokemon(url).then(datas => {
            //setPokemonInfos(datas.ablilities);
            setPokemonImg(datas.sprites.front_default);
        })
    }

    return (

        <View style={styles.container}>
            {
                PokemonImg ? (
                        <>
                            <Text style={{textAlign: "center"}}>{Capitalize(props.name)}</Text>
                            <Image source={{uri: PokemonImg}}
                                   style={{width: '100%', height: "100%"}}/>
                            {/*<FlatList
                                data={PokemonInfos}
                                keyExtractor={(item => {
                                    item.ability.name
                                })}
                                renderItem={({item}) =>
                                    <Text>{item.ability.name}</Text>}
                            />*/}
                        </>
                    )
                    : <>
                        <Text style={{textAlign: "center"}}>{Capitalize(props.name)}</Text>
                        <Image source={pokeball}
                               style={{width: '100%', height: "100%"}}/>
                    </>
            }
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
    },
});