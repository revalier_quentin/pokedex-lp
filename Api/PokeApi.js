export async function getPokemon(url = "https://pokeapi.co/api/v2/pokemon"){
    return await fetch(url)
        .then((response) => response.json())
        .catch((error) => console.log("error : ", error));
}
